import m from './multiply';
import proxy from './proxy';

function square(n: number): number {
  return m.multiply(n, n);
}

export default proxy({square});
