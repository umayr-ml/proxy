
import s from './square';
import m from './multiply';

for (let i of [1,2,3,5,6,7]) {
  m.multiply(i, i);
}

for (let i of [1,2,3,5,6,7,8,8,1,2,5,3,4,2,3,4,5,6,7,8,9]) {
  s.square(i);
}
