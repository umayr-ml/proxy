let cache = {};

export default function proxy(obj) {
  let handler = {
    get(target, propKey, receiver) {
      const fn = target[propKey];
      return function (...args) {
        let key = `${propKey}#${JSON.stringify(args)}`;
        if (cache[key] === void 0) {
          console.log('NO CACHE: %s', key);
          cache[key] = fn.apply(this, args);
        } else {
          console.log('CACHE: %s', key);
        }
        return cache[key];
      };
    }
  };
  return new Proxy(obj, handler);
}
