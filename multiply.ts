import proxy from './proxy';

function multiply(n: number, m: number): number {
  return n * m;
}

export default proxy({
  multiply,
});
